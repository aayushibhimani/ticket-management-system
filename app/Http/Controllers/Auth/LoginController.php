<?php

namespace App\Http\Controllers\Auth;
use Auth;
use App\Http\Controllers\Controller;
use App\LoginHours;
use App\Providers\RouteServiceProvider;
use App\User;
use Carbon\Carbon;
use GuzzleHttp\Psr7\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth as FacadesAuth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;


    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    
    }
    public function authenticated(\Illuminate\Http\Request $request, $user)
    {
        // dd($user);
        $user->update([
            'loged_in_at' =>Carbon::now()->toDateTimeString()
        ]);

        $user->calculateEfficiency();
    }
     public function logout() {
         $user = auth()->user();
         $user->update([
            'loged_out_at' =>Carbon::now()->toDateTimeString()
        ]); 

        $login = explode(" ", $user->loged_in_at);
        $login_time = explode(":", $login[1]);
        $loged_in_at = ($login_time[0] + ($login_time[1]/60) +($login_time[2]/3600) );
    
        $logout = explode(" ", $user->loged_out_at);
        $logout_time = explode(":", $logout[1]);
        $loged_out_at = ($logout_time[0] + ($logout_time[1]/60) +($logout_time[2]/3600));
        $hours =  $loged_out_at - $loged_in_at;

        LoginHours::where('user_id', $user->id)->update(['hours'=> $hours]);
        
        Auth::logout();
        return redirect('login');
      }
}
