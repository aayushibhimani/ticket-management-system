<?php

namespace App\Http\Controllers;

use App\LoginHours;
use App\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

       $best_employee = User::where('role', 'Employee')->orderBy('points', 'desc')->first();
        if($best_employee->points === 0)
        {
            $employees = User::where('role', 'Employee')->get();
            foreach($employees as $employee)
            {
                $emp_id[] = $employee->id;
            }
            $login_hours= LoginHours::whereIn('id', $emp_id)->orderBy('hours','desc')->first();
            $best_emp = User::where('id', $login_hours->user_id)->get();
            $best_employee = $best_emp[0];
        }
        

        $best_manager = User::where('role', 'Project Manager')->orderBy('points', 'desc')->first();
        // dd($best_manager);
        if($best_manager->points === 0)
        {
            $managers = User::where('role','Project Manager')->get();
            foreach($managers as $manager)
            {
                $manager_id[]= $manager->id;
            }
            $login_hours = LoginHours::where('id', $manager_id)->orderBy('hours','desc')->first();
            $best_mng = User::where('id', $login_hours->user_id)->get();
            $best_manager = $best_mng[0];
            // dd($best_manager);
        }
        return view('home', compact(['best_employee', 'best_manager']));

    }
}
