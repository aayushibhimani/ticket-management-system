<?php

namespace App\Http\Controllers;

use App\Http\Requests\Projects\CreateProjectRequest;
use App\User;
use App\Project;
use Illuminate\Http\Request;

class ProjectsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //  dd(auth()->user()->id);
        $user = auth()->user();
        $projects = Project::all();
        if($user->isAdmin())
        {
            return view('projects.index', compact(['projects']));
        }
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users =  User::where('assigned', '0')->get();
        return view('projects.create', compact(['users']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, User $user)
    {
        //
        // dd($request->manager_id);
        $project= Project::create([
            'title'=>$request->title,
            'overview'=> $request->overview,
        ]);

        $project->users()->attach($request->manager_id);
        $project->users()->attach($request->team);
        
        $manager_id =$request->manager_id;
        $team_members = $request->team;
        
        User::where('id', $manager_id)->update(['assigned'=>'1', 'role'=>'Project Manager']);
        User::whereIn('id', $team_members)->update(['assigned'=>'1']);

        session()->flash('success','Project Created Successfully');
        return redirect(route('projects.index'));
    }

    // public function updateUser(Request $request, User $user)
    // {
         
    //     dd($request->manager_id);
    // }

    /**
     * Display the specified resource.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function show(Project $project)
    {   
        $project = $project;
        $users = $project->users()->get();
        return view('projects.show', compact([
                'users','project'
            ]));
    }
    public function detail(User $user)
    {
        $project_details = $user->project()->get();
        $project = $project_details;
        // dd($project);
        if (!$project_details->isEmpty())
        {
            // dd($project_details);
            $project = $project_details[0];
            $users = $project->users()->get();
            return view('projects.show', compact([
                'users', 'project'
            ]));
        }
        else{
            $project = [];
            return view('projects.show', compact([
             'project'
        ]));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function edit(Project $project)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Project $project)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function destroy(Project $project)
    {
        //
    }

}
