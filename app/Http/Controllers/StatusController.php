<?php

namespace App\Http\Controllers;

use App\Project;
use App\Task;
use App\User;
use Illuminate\Http\Request;

class StatusController extends Controller
{
    public function projectStatus(Request $request, Project $project)
    {
        // dd($request->status);
        Project::where('id', $project->id)->update(['status'=>$request->status]);
        return redirect()->back();
    }
    public function projectFinalStatus(Project $project)
    {
        // dd($project->status);
        if($project->status ==='Done')
        {
            Project::where('id', $project->id)->update(['status'=>'Completed']);
            $users = $project->users()->get();
            foreach($users as $user){
                $user_id[] = $user->id;
                if($user->role === 'Project Manager'){
                    $manager_id = $user->id;
                }
            }
            User::whereIn('id', $user_id)->update(['assigned'=>'0']);
            User::whereIn('id', $user_id)->update(['role'=>'Employee']);
            $manager= User::where('id', $manager_id)->get();
            $points = $manager[0]->points;
            $new_points = $points + 5;
            User::where('id', $manager_id)->update(['points'=>$new_points]);
        }
        return redirect()->back();
    }
    public function taskStatus(Request $request, Task $task)
    {
        $user = User::where('id', auth()->user()->id)->get();
        // dd($request->status);
        Task::where('id', $task->id)->update(['status'=>$request->status]);
        return redirect()->back();
    }

    public function taskFinalStatus(Task $task)
    {
        $user = $task->user()->get();
        $tasks_count = $user[0]->tasks_count;
        // dd($user[0]->points);
        if($task->status ==='Done')
        {
            Task::where('id', $task->id)->update(['status'=>'Completed']);
            $tasks_count = $tasks_count - 1;
            User::where('id', $user[0]->id)->update(['tasks_count'=> $tasks_count]);
            $current_points = $user[0]->points;
            $current_tasks_completed = $user[0]->tasks_completed;
            $tasks_completed = $current_tasks_completed +1;
            $new_points = $current_points +1;
            // dd($new_points);
            User::where('id', $user[0]->id)->update(['points'=>$new_points, 'tasks_completed'=>$tasks_completed]);
        }
        return redirect()->back();
    }

}
