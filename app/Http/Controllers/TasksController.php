<?php

namespace App\Http\Controllers;

use App\Project;
use App\Task;
use App\User;
use Illuminate\Http\Request;

class TasksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = auth()->user();
        // dd($user);
        $project_details = $user->project()->get();
        $project = $project_details[0];
        $users = $project->users()->where('role','employee')->get();
        // $users = $users_details[0];
        return view('tasks.index', compact(['project','users']));
    }

    public function details(User $user)
    {
        // dd($user);
        $tasks = Task::where('user_id', $user->id)->get();
        // dd($tasks);
        return view('tasks.details', compact(['tasks']));
    }
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(User $user)
    {
        return view('tasks.create', compact(['user']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->user_id);
        $user_details = User::where('id', $request->user_id)->get();
        $user = $user_details[0];
        $project = $user->project()->get();
        $project_id = $project[0]->id;

        $project= Task::create([
            'project_id'=>$project_id,
            'user_id'=>$user->id,
            'title'=>$request->title,
            'description'=> $request->description,
        ]);

        $task_count = $user->tasks_count;
        $user->updateTasksCount($task_count, $user->id);

        session()->flash('success','Task Reassigned Successfully');
        return redirect(route('tasks.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function show(Task $task)
    {
        // dd($task);
        $user_details = User::where('id', $task->user_id)->get();
        $user = $user_details[0];
        return view('tasks.show', compact(['task', 'user']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function edit(User $task)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Task $task)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function destroy(Task $task)
    {
        //
    }
    public function reassign(Task $task)
    {
        $project_details = $task->project()->get();
        $project = $project_details[0];
        $users = $project->users()->get();
        // dd($users);
        return view('tasks.reassign', compact(['task', 'users']));
    }
}
