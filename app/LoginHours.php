<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LoginHours extends Model
{
    protected $fillable = [
        'user_id', 'hours'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    
}
