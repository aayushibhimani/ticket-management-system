<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    //
    protected $fillable = [
        'title' , 'overview' ,'status'
    ];

    // public function users()
    // {
    //     return $this->hasMany(User::class);
    // }

    public function users()
    {
        return $this->belongsToMany(User::class)->withTimestamps();
    }
    public function tasks()
    {
        return $this->hasMany(Task::class);
    }



}
