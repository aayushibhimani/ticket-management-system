<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','role','loged_in_at','loged_out_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    //helper classes
    public function isEmployee()
    {
        return $this->role === 'Employee';
    }
    public function isManager()
    {
        return $this->role === 'Project Manager';
    }
    public function isAdmin()
    {
        return $this->role === 'Admin';
    }
    public function authorizedUser()
    {
        return auth()->user();
    }

    public function updateTasksCount(int $task_count, int $user_id)
    {
        $task_count= $task_count+1;
        $answer= User::where('id', $user_id)->update(['tasks_count'=> $task_count]);

    }
    public function calculateEfficiency()
    {
        $user = auth()->user();
        $hours = LoginHours::where('user_id', $user->id)->get();
        // dd($hours[0]->hours);
        if($hours[0]->hours % 60 == 0){
            $current_points = $user->points;
            $points = $current_points + 1;
            $user->update([
                'points'=>$points
            ]);
        }
    }
    // Relationships
    public function project()
    {
        return $this->belongsToMany(Project::class);
    }
    public function tasks()
    {
        return $this->hasMany(Task::class);
    }
    public function loginHours()
    {
        return $this->belongsTo(LoginHours::class);
    }

}
