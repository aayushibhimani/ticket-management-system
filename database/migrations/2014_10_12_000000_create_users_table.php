<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('role')->default('Employee');
            $table->unsignedInteger('tasks_count')->default(0);
            $table->unsignedInteger('tasks_completed')->default(0);
            $table->unsignedInteger('assigned')->default(0);
            $table->dateTime('loged_in_at')->nullable();
            $table->dateTime('loged_out_at')->nullable();
            $table->unsignedInteger('points')->default(0);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
