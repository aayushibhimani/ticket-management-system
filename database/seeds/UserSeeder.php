<?php

use App\LoginHours;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::where('email', 'aayushibhimani@gmailcom')->get()->first();
        if(!$user)
        {
            User::create([
                'name'=>'Aayushi Bhimani',
                'email'=>'aayushibhimani@gmail.com',
                'password'=>Hash::make('abcd1234'),
                'role'=>'Admin'
            ]);
            LoginHours::create([
                'user_id'=> 1,
                'hours'=>0
            ]);
        }else{
            $user->update(['role'=>'Admin']);
        }

        User::create([
            'name'=>'John Doe',
            'email'=>'johndoe@gmail.com',
            'password'=>Hash::make('abcd1234'),
        ]);
        LoginHours::create([
            'user_id'=> 2,
            'hours'=>0
        ]);
        User::create([
            'name'=>'Jimmy Doe',
            'email'=>'jimmydoe@gmail.com',
            'password'=>Hash::make('abcd1234'),
            'assigned'=>1
        ]);
        LoginHours::create([
            'user_id'=> 3,
            'hours'=>0
        ]);
        User::create([
            'name'=>'Jane Doe',
            'email'=>'janedoe@gmail.com',
            'password'=>Hash::make('abcd1234'),
        ]);
        LoginHours::create([
            'user_id'=> 4,
            'hours'=>0
        ]);
        User::create([
            'name'=>'Vatsal Bhimani',
            'email'=>'vatsalbhimani@gmail.com',
            'password'=>Hash::make('abcd1234'),
            'role'=>'Project Manager'
        ]);
        LoginHours::create([
            'user_id'=> 5,
            'hours'=>0
        ]);
        User::create([
            'name'=>'Clara Hudson',
            'email'=>'clara@gmail.com',
            'password'=>Hash::make('abcd1234')
        ]);
        LoginHours::create([
            'user_id'=> 6,
            'hours'=>0
        ]);
        User::create([
            'name'=>'Angel Bishop',
            'email'=>'angel@gmail.com',
            'password'=>Hash::make('abcd1234')
        ]);
        LoginHours::create([
            'user_id'=> 7,
            'hours'=>0
        ]);
        User::create([
            'name'=>'Sue Lynch',
            'email'=>'sue@gmail.com',
            'password'=>Hash::make('abcd1234')
        ]);
        LoginHours::create([
            'user_id'=> 8,
            'hours'=>0
        ]);
        User::create([
            'name'=>'Howard Hughes',
            'email'=>'howard@gmail.com',
            'password'=>Hash::make('abcd1234')
        ]);
        LoginHours::create([
            'user_id'=> 9,
            'hours'=>0
        ]);
        User::create([
            'name'=>'Karl Moreno',
            'email'=>'karl@gmail.com',
            'password'=>Hash::make('abcd1234')
        ]);
        LoginHours::create([
            'user_id'=> 10,
            'hours'=>0
        ]);
        User::create([
            'name'=>'Beth Willis',
            'email'=>'beth@gmail.com',
            'password'=>Hash::make('abcd1234')
        ]);
        LoginHours::create([
            'user_id'=> 11,
            'hours'=>0
        ]);
        User::create([
            'name'=>'Tina Parker',
            'email'=>'tina@gmail.com',
            'password'=>Hash::make('abcd1234')
        ]);
        LoginHours::create([
            'user_id'=> 12,
            'hours'=>0
        ]);
        User::create([
            'name'=>'Gilbert Fields',
            'email'=>'gilbert@gmail.com',
            'password'=>Hash::make('abcd1234')
        ]);
        LoginHours::create([
            'user_id'=> 13,
            'hours'=>0
        ]);
        User::create([
            'name'=>'Nelson Reid',
            'email'=>'nelsonl@gmail.com',
            'password'=>Hash::make('abcd1234')
        ]);
        LoginHours::create([
            'user_id'=> 14,
            'hours'=>0
        ]);
        User::create([
            'name'=>'Jenny Robinson',
            'email'=>'jenny@gmail.com',
            'password'=>Hash::make('abcd1234')
        ]);
        LoginHours::create([
            'user_id'=> 15,
            'hours'=>0
        ]);

    }
}
