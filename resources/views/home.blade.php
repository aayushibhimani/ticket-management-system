@extends('layouts.app')

@section('page-level-styles')
<!-- Custom fonts for this template-->
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.1/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <style>
    .text-grey{
      color: #dddddd;
    }
  </style>
@endsection

@section('content')

<div class="container">
                @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                @endif
<div class="row justify-content-center">
        <div class="col-md-6 p-0">
            <div class="card m-2">
                <div class="card-body">
                        <h4 class="text-center font-weight-bold text-secondary p-2"> Employee Of the Month</h4>
                        <div class="text-center p-2"><img src="{{ Gravatar::src($best_employee->email) }}" alt="" class ="rounded-circle "></div>
                        <h5 class="font-weight-bold text-center text-secondary p-2">{{$best_employee->name}}</h5>
                        <h5 class="text-bold text-center text-secondary p-2">{{$best_employee->email}}</h5>
                </div>
            </div>
        </div>
        <div class="col-md-6 p-0">
            <div class="card m-2">
                <div class="card-body ">
                <h4 class="text-center font-weight-bold text-secondary p-2"> Manager Of the Month</h4>
                        <div class="text-center p-2"><img src="{{ Gravatar::src($best_manager->email) }}" alt="" class ="rounded-circle "></div>
                        <h5 class="font-weight-bold text-center text-secondary p-2">{{$best_manager->name}}</h5>
                        <h5 class="text-bold text-center text-secondary p-2">{{$best_manager->email}}</h5>
                </div>
            </div>
        </div>
        </div>
    

<!-- Content Row -->
<div class="row justify-content-between mt-4 ">

<div class="col-md-4 ">
  <div class="card shadow mb-4">
    <div class="card-body border-left h-100">
        <div class="card-body p-0">
            <div class="row no-gutters align-items-center">
              <div class="col mr-1">
                <div class="text-xs font-weight-bold uppercase mb-1 text-danger">EMPLOYEES WORKING</div>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-md-9 mr-1">
                <div class="h5 mb-0 font-weight-bold text-secondary">125</div>
              </div>
              <div class="col-auto  pb-2 pl-2">
                      <i class="fas fa-users fa-2x text-grey"></i>
                    </div>
            </div>
        </div>
    </div>
  </div>
</div>

<div class="col-md-4">
  <div class="card shadow mb-4">
    <div class="card-body border-left h-100">
        <div class="card-body p-0">
            <div class="row no-gutters align-items-center">
              <div class="col mr-1">
                <div class="text-xs font-weight-bold text-info uppercase mb-1">TASKS</div>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-md-3 mr-1">
                <div class="h5 mb-0 font-weight-bold text-secondary">50%</div>
              </div>
              <div class="col-md-6 mr-1">
              <div class="progress progress-sm mr-2" style="height:8px;">
                 <div class="progress-bar bg-info" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
              </div>
              </div>
              <div class="col-auto pb-2 pl-2">
              <i class="fa fa-clipboard-list text-grey fa-2x"></i>
              </div>
            </div>
        </div>
    </div>
  </div>
</div>

<div class="col-md-4">
  <div class="card shadow mb-3">
    <div class="card-body border-left h-100">
        <div class="card-body p-0">
            <div class="row no-gutters align-items-center">
              <div class="col mr-1">
                <div class="text-xs font-weight-bold uppercase mb-1 text-success">PROJECTS COMPLETED</div>
              </div>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col-md-9 mr-1">
                <div class="h5 mb-0 font-weight-bold text-secondary">20</div>
              </div>
              <div class="col-auto  pb-2 pl-2">
                      <i class="fa fa-laptop fa-2x text-grey"></i>
                    </div>
            </div>
        </div>
    </div>
  </div>
</div>

</div>
</div>

<script>
  function checkFirstVisist(){
    if(document.cookie.indexOf('mycookie')==-1) {
    // cookie doesn't exist, create it now
    alert('created');
  }
  else {
    // not first visit, so alert
    alert('You refreshed!');
  }
  }
</script>

@endsection













