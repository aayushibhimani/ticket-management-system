@extends('layouts.app')
@section('page-level-styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/trix/1.2.3/trix.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
    
@endsection
@section('content')
    <div class="card">
        <div class="card-header">Add Project</div>
        <div class="card-body">
            <form action="{{ route('projects.store') }}" method="POST">
                @csrf
                <div class="form-group">
                    <label for="title">Title</label>
                    <input type="text"
                            value="{{old('title')}}"
                            class="form-control @error('title') is-invalid @enderror"
                            name="title" id="title">
                    @error('title')
                        <p class="text-danger">{{$message}}</p>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="overview">Overview</label>
                    <input id="overview" type="hidden" name="overview">
                    <trix-editor input="overview"></trix-editor>
                    <!-- <textarea class="form-control @error('overview') is-invalid @enderror"
                            name="overview" id="overview" rows="4">{{old('overview')}}</textarea> -->
                    @error('overview')
                        <p class="text-danger">{{$message}}</p>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="manager">Project Manager</label>
                    <select name="manager_id" id="manager_id" class="form-control">
                        @foreach($users as $user)
                            <option value="{{$user->id}}">{{$user->name}}</option>
                        @endforeach
                    </select>
                    @error('users')
                        <p class="text-danger font-weight-light">{{$errors->first('users')}}</p>
                    @enderror    
                </div>
                <div class="form-group">
                    <label for="team[]">Team</label>
                    <select name="team[]" id="team" class="form-control select2" multiple>
                        @foreach($users as $user)
                            <option value="{{$user->id}}">{{$user->name}}</option>
                        @endforeach
                    </select>
                    @error('users')
                        <p class="text-danger font-weight-light">{{$errors->first('users')}}</p>
                    @enderror    
                </div>
                <div class="form-group">
                    <button class="btn btn-success" type="submit">Add Project</button>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('page-level-scripts')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/trix/1.2.3/trix.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    <script>
        flatpickr("#published_at", {
            enableTime: true
        });
    $(document).ready(function(){
            $('.select2').select2();
        });
    </script>
@endsection
