@extends('layouts.app')

@section('content')
    <div class="d-flex justify-content-end mb-3">
            <a href="{{ route('projects.create') }}"  class="btn btn-primary">New Project</a>
    </div> 

    <div class="card">
        <div class="card-header">Projects</div>

        <div class="card-body">
            <table class="table table-bordered">
                <thead>
                    <th>Name</th>
                    <th>Status</th>
                    <th>Details</th>
                    <th>Action</th>
                </thead>
                <tbody>
                    @foreach($projects as $project)
                        <tr>
                            <td>
                                {{$project->title}}
                            </td>
                            <td>
                                {{$project->status}}
                            </td>
                            <td>
                                <a href="{{route('projects.show', $project->id)}}">Details</a>
                            </td>
                            <td>
                                @if($project->status === 'Done')
                                <a href="{{route('projects.final.status', $project->id)}}" class="btn btn-outline-danger">Completed</a>
                                @endif
                                @if(!($project->status === 'Completed'))
                                <a href="" class="btn btn-outline-success">Reassign</a>
                                @endif
                            </td>
                        </tr>
                      @endforeach  
                </tbody>
            </table>
        </div>
    </div>
@endsection