@extends('layouts.app')

@section('page-level-styles')
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
@endsection

@section('content')
<div class="card">

@if($project)
    <div class="card-header">
    <div class="row">
    <div class="col-md-9"><h5 class="text-primary text-align-center p-2">Project Details</h5></div>
    <div class="col-md-3">
        @if($project->status === 'Completed')
            <h5 class="text-danger text-right p-2">Completed</h5>
        @endif
    </div>
    </div>
    </div> 
    <div class="card-body">
        <h2>{{$project->title}}</h2>
        <br>
        <h4>Overview</h4>
        <p>{!!$project->overview!!}</p>
        <h4>Project Manager</h4>
        @foreach($users as $user)
        @if($user->role === 'Project Manager')
                <p>{{$user->name}}</p>
        @endif
        @endforeach
        <h4>Team Members</h4>
        <ul>
        @foreach($users as $user)
        @if(!($user->role === 'Project Manager'))
                <li>{{$user->name}}</li>
        @endif
        @endforeach
        </ul>
        @if(auth()->user()->role === 'Project Manager')
            <a href="{{route('tasks.index')}}" class="btn btn-primary">Task Details</a>
        @endif
        @if(auth()->user()->role === 'Employee')
        <a href="{{route('tasks.details',auth()->user()->id)}}" class="btn btn-primary">Task Details</a>
        @endif

        <br>
        <br>    
        
        @if(!($project->status === 'Completed'))
        <h4>Status</h4>
        <form action="{{route('projects.status', $project->id)}}" method="POST">
            @csrf   
            @method('PUT')
            <button type="submit" class="btn {{$project->status ==='Done' ? 'text-success' : ''}}" onclick="addClassOnClick(this)" value="Done">
            <div class="form-check">
                <input class="form-check-input" type="hidden" name="status" id="status" value="Done">
                <label class="form-check-label"><i class="fa fa-check">Done</i>
            </div>
            </button>
        </form>

        <form action="{{route('projects.status', $project->id)}}" method="POST">
            @csrf   
            @method('PUT')
            <button type="submit" class="btn {{$project->status ==='onGoing' ? 'text-success' : ''}}" onclick="addClassOnClick(this)" value="onGoing">
            <div class="form-check">
                <input class="form-check-input" type="hidden" name="status" id="status" value="onGoing">
                <label class="form-check-label"><i class="fa fa-check">onGoing</i>
            </div>
            </button>
        </form>
        
        <form action="{{route('projects.status', $project->id)}}" method="POST">
            @csrf   
            @method('PUT')
            <button type="submit" class="btn {{$project->status ==='Give Up' ? 'text-success' : ''}}" onclick="addClassOnClick(this)" value="Give Up">
            <div class="form-check">
                <input class="form-check-input" type="hidden" name="status" id="status" value="Give Up">
                <label class="form-check-label"><i class="fa fa-check">Give Up</i>
            </div>
            </button>
        </form>

        @endif
    @else
    <h3 class="text-secondary p-5">You are not a part of any project as of now</h3>

    </div>
    </div>
@endif

<script>
    function addClassOnClick(button){
        $(button).addClass("text-success")
    }
</script>
@endsection