@extends('layouts.app')
@section('page-level-styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/trix/1.2.3/trix.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
    
@endsection

@section('content')
<div class="card">
    <div class="card-header">Add Task</div>
    <div class="card-body">
        <form action="{{route('tasks.store')}}" method="POST">
            @csrf   
            <input type="hidden" value="{{$user->id}}" name="user_id">
            <div class="form-group">
                    <label for="name">Assign To</label>
                    <input type="text"
                            value="{{$user->name}}"
                            class="form-control @error('name') is-invalid @enderror"
                            name="name" id="name">
                    @error('name')
                        <p class="text-danger">{{$message}}</p>
                    @enderror
            </div>   

            <div class="form-group">
                    <label for="title">Title</label>
                    <input type="text"
                            value="{{old('title')}}"
                            class="form-control @error('title') is-invalid @enderror"
                            name="title" id="title">
                    @error('title')
                        <p class="text-danger">{{$message}}</p>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="description">Description</label>
                    <input id="description" type="hidden" name="description">
                    <trix-editor input="description"></trix-editor>
                    <!-- <textarea class="form-control @error('description') is-invalid @enderror"
                            name="description" id="description" rows="4">{{old('description')}}</textarea> -->
                    @error('description')
                        <p class="text-danger">{{$message}}</p>
                    @enderror
                </div>  

                <div class="form-group">
                    <button class="btn btn-success" type="submit">Add Task</button>
                </div>
        </form>
    </div>
</div>
@endsection

@section('page-level-scripts')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/trix/1.2.3/trix.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    <script>
        flatpickr("#published_at", {
            enableTime: true
        });
    $(document).ready(function(){
            $('.select2').select2();
        });
    </script>
@endsection
