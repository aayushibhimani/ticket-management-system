@extends('layouts.app')

@section('content')
    @if(auth()->user()->role === 'Project Manager')
    <div class="d-flex justify-content-end mb-3">
    <a href="{{ route('tasks.create', $tasks[0]->user_id) }}"  class="btn btn-primary">Assign Task</a>
    </div> 
    @endif
    <div class="card">
        <div class="card-header">Tasks</div>
    @if(!$tasks->isEmpty())
        <div class="card-body">
            <table class="table table-bordered">
                <thead>
                    <th>Name</th>
                    <th>Status</th>
                    <th>Details</th>
                    @if(auth()->user()->role === 'Project Manager')
                    <th>Action</th>
                    @endif
                </thead>
                <tbody>
                    @foreach($tasks as $task)
                        <tr>
                            <td>
                                {{$task->title}}
                            </td>
                            <td>
                                {{$task->status}}
                            </td>
                            <td>
                                <a href="{{route('tasks.show', $task->id)}}">Details</a>
                            </td>
                           
                            @if(auth()->user()->role === 'Project Manager') 
                            <td>
                                @if($task->status === 'Done')
                                <a href="{{route('tasks.final.status', $task->id)}}" class="btn btn-outline-danger">Completed</a>
                                @endif
                                @if(!($task->status === 'Completed'))
                                <a href="{{route('tasks.reassign', $task->id)}}" class="btn btn-outline-success">Reassign</a>
                                @endif
                            </td>
                            @endif
                        </tr>
                      @endforeach  
                </tbody>
            </table>
            @else
            <h3 class="text-secondary p-5">No Tasks</h3>
        </div>
        @endif
    </div>
@endsection