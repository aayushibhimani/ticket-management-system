@extends('layouts.app')

@section('content')

    <div class="card">
        <div class="card-header">Task Details</div>

        <div class="card-body">
            <table class="table table-bordered">
                <thead>
                    <th>Name</th>
                    <th>Project Name</th>
                    <th>Tasks Assigned</th>
                    <th>Action</th>
                    <th>Details</th>
                </thead>
                <tbody>
                    @foreach($users as $user)
                        <tr>
                            <td>
                                {{$user->name}}
                            </td>
                            <td>
                                {{$project->title}}
                            </td>
                            <td>
                                {{$user->tasks_count}}
                            </td>
                            <td>
                                <a href="{{ route('tasks.create', $user->id) }}" class="btn btn-outline-primary">Assign Task</a>
                            </td>
                            <td>
                             @if($user->tasks_count)
                            <a href="{{ route('tasks.details', $user->id)}}" class="btn btn-outline-danger">Details</a>
                            @endif
                            </td>
                            
                        </tr>
                      @endforeach  
                </tbody>
            </table>
        </div>
    </div>
@endsection
