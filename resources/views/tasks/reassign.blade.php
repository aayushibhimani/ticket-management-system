@extends('layouts.app')
@section('page-level-styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/trix/1.2.3/trix.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
    
@endsection

@section('content')
<div class="card">
    <div class="card-header">Reassign Task</div>
    <div class="card-body">
        <form action="{{route('tasks.store')}}" method="POST">
            @csrf   
            <div class="form-group">
                    <label for="name">Assign to</label>
                    <select name="user_id" id="user_id" class="form-control">
                        @foreach($users as $user)
                            @if($user->role === 'Employee')
                                <option value="{{$user->id}}">{{$user->name}}</option>
                            @endif
                        @endforeach
                    </select>
                    @error('users')
                        <p class="text-danger font-weight-light">{{$errors->first('users')}}</p>
                    @enderror    
                </div> 

            <div class="form-group">
                    <label for="title">Title</label>
                    <input type="text"
                            value="{{$task->title}}"
                            class="form-control @error('title') is-invalid @enderror"
                            name="title" id="title">
                    @error('title')
                        <p class="text-danger">{{$message}}</p>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="description">Description</label>
                    <input id="description" type="hidden" name="description" value="{{$task->description}}">
                    <trix-editor input="description"></trix-editor>
                    <!-- <textarea class="form-control @error('description') is-invalid @enderror"
                            name="description" id="description" rows="4">{{old('description')}}</textarea> -->
                    @error('description')
                        <p class="text-danger">{{$message}}</p>
                    @enderror
                </div>  

                <div class="form-group">
                    <button class="btn btn-success" type="submit">Reassign Task</button>
                </div>
        </form>
    </div>
</div>
@endsection

@section('page-level-scripts')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/trix/1.2.3/trix.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    <script>
        flatpickr("#published_at", {
            enableTime: true
        });
    $(document).ready(function(){
            $('.select2').select2();
        });
    </script>
@endsection
