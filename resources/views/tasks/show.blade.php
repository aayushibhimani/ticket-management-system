@extends('layouts.app')

@section('page-level-styles')
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
@endsection

@section('content')
<div class="card">
<div class="card-header">
<div class="row">
<div class="col-md-9"><h5 class="text-primary text-align-center p-2">Task Details</h5></div>
<div class="col-md-3">
    @if($task->status === 'Completed')
        <h5 class="text-danger text-right p-2">Completed</h5>
    @endif
</div>
</div>
</div> 
<div class="card-body">
    <h2>{{$task->title}}</h2>
    <br>
    <h4>Description</h4>
    <p>{!!$task->description!!}</p>
    
    @if($user->id === auth()->user()->id ) 
    @if(!($task->status === 'Completed'))
    <h4>Status</h4>
    <form action="{{route('tasks.status', $task->id)}}" method="POST">
        @csrf   
        @method('PUT')
        <button type="submit" class="btn {{$task->status ==='Done' ? 'text-success' : ''}}" onclick="addClassOnClick(this)" value="Done">
        <div class="form-check">
            <input class="form-check-input" type="hidden" name="status" id="status" value="Done">
            <label class="form-check-label"><i class="fa fa-check">Done</i>
        </div>
        </button>
    </form>

    <form action="{{route('tasks.status', $task->id)}}" method="POST">
        @csrf   
        @method('PUT')
        <button type="submit" class="btn {{$task->status ==='onGoing' ? 'text-success' : ''}}" onclick="addClassOnClick(this)" value="onGoing">
        <div class="form-check">
            <input class="form-check-input" type="hidden" name="status" id="status" value="onGoing">
            <label class="form-check-label"><i class="fa fa-check">onGoing</i>
        </div>
        </button>
    </form>
    
    <form action="{{route('tasks.status', $task->id)}}" method="POST">
        @csrf   
        @method('PUT')
        <button type="submit" class="btn {{$task->status ==='Give Up' ? 'text-success' : ''}}" onclick="addClassOnClick(this)" value="Give Up">
        <div class="form-check">
            <input class="form-check-input" type="hidden" name="status" id="status" value="Give Up">
            <label class="form-check-label"><i class="fa fa-check">Give Up</i>
        </div>
        </button>
    </form>

    @endif
    @endif
</div>
</div>

<script>
    function addClassOnClick(button){
        $(button).addClass("text-success")
    }
</script>
@endsection