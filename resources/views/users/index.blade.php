@extends('layouts.app')

@section('content')

    <div class="d-flex justify-content-end mb-3">
            <a href="{{route('bestEmployee')}}" class="btn btn-outline-primary mr-3">Best Employee</a>
            <a href="{{route('register')}}" class="btn btn-primary">Add User</a>
    </div>
    <div class="card">
        <div class="card-header">Users</div>

        <div class="card-body">
            <table class="table table-bordered">
                <thead>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Role</th>
                </thead>
                <tbody>
                    @foreach($users as $user)
                        <tr>
                            <td>
                                {{$user->name}}
                            </td>
                            <td>
                                {{$user->email}}
                            </td>
                            <td>
                                {{$user->role}}
                            </td>
                        </tr>
                      @endforeach  
                </tbody>
            </table>
        </div>
    </div>
@endsection