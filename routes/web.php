<?php

use App\Http\Controllers\ProjectsController;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','WelcomeController@index');


Auth::routes();

Route::middleware(['auth','employee','manager'])->group(function(){
      // Route::get('/users','UsersController@index')->name('users.index') ;
      Route::resource('users','UsersController')->except('bestEmployee');
      Route::get('users/hello', 'UsersController@bestEmployee')->name('bestEmployee');

});
Route::middleware(['auth'])->group(function(){
      Route::resource('projects', 'ProjectsController')->except('detail');
Route::get('projects/detail/{user}', 'ProjectsController@detail')->name('projects.detail');

Route::put('projects/status/{project}', 'StatusController@projectStatus')->name('projects.status');
Route::get('projects/status/{project}', 'StatusController@projectFinalStatus')->name('projects.final.status');
Route::put('tasks/status/{task}', 'StatusController@taskStatus')->name('tasks.status');
Route::get('tasks/status/{task}', 'StatusController@taskFinalStatus')->name('tasks.final.status');


Route::resource('tasks','TasksController')->except('create','detailing');
// Route::get('tasks/{user}', 'TasksController@show')->name('tasks.show');
Route::get('tasks/details/{user}', 'TasksController@details')->name('tasks.details');
Route::get('tasks/{user}/create', 'TasksController@create')->name('tasks.create');
Route::get('tasks/{task}/reassign', 'TasksController@reassign')->name('tasks.reassign');

Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('home');
});



